import java.util.*;
import java.io.*;

// テスト用クラス
// 実行例: java Eye [filename]
class Eye{
	public static void main(String[] args){
		EyeBlink eyeBlink = new EyeBlink(args[0], 10);
		eyeBlink.analyze();
	}
}

// TODO: インスタンスの動きを見直す
// 暫定版
class EyeBlink{
	private final List<Double> areaList = new ArrayList<Double>();			// 面積を格納するリスト
	private final List<Double> differenceList = new ArrayList<Double>();	// 差分値を格納するリスト
	private final Map<Integer, List<Double>> eyeBlinksMap = new TreeMap<>();// <key, value> → <サンプル点, keyに指定されたサンプル点から始まる瞬目波形>

	private int test = 0;

	private String fileName;
	private File dataFile;
	private int n_of_sample = 10;											// 平均を計算する先頭からのサンプル数
	private double avg_EyeArea;
	private double avg_Diff;
	private double stdDev_EyeArea;
	private double p = 0.7;
	private double sigma = 4;
	private double Th1;
	private double Th2;
	private double Th_eyeArea;

	public EyeBlink(String fileName){
		this(fileName, 10);
	}
	public EyeBlink(String fileName, int number_of_sample){
		this.fileName = fileName;
		System.out.println(fileName);
		this.dataFile = new File(fileName);
		this. n_of_sample = number_of_sample;
	}

	// TODO: 名前の変更
	public void setP(double p){
		this.p = p;
	}
	public void setSigma(double sigma){
		this.sigma = sigma;
	}

	public List<Double> getAreaList(){
		return this.areaList;
	}
	public List<Double> getDifferenceList(){
		return this.differenceList;
	}
	public Map<Integer, List<Double>> getEyeBlinksMap(){
		return this.eyeBlinksMap;
	}
	public double getP(){
		return this.p;
	}
	public double getSigma(){
		return this.sigma;
	}
	public String getFileName(){
		return this.dataFile.getName();
	}

	// 瞬目波形の検出を行うメソッド パラメータ等設定変更の後、繰り返し実行も可
	public EyeBlink analyze(){
		// 初期化
		this.clear();

		this.loadFile();
		this.calcParam();
		this.detect();
		this.print();

		return this;
	}

	private void clear(){
		this.areaList.clear();
		this.differenceList.clear();
		this.eyeBlinksMap.clear();
	}

	// TODO: データのフォーマットを指定出来るように
	private void loadFile(){
		//---- ファイルから面積を読み込み、リストに格納 ----//
		try( Scanner stdIn = new Scanner(this.dataFile) ){

			if(!stdIn.hasNextDouble()) stdIn.nextLine();			// 数値でなければ一行読み飛ばし
			while(stdIn.hasNextDouble()){
				areaList.add( Double.valueOf(stdIn.nextLine().split("\t")[1]));	// タブで分割しDouble型に変換後、リストに追加する
			}
			for(int i_al = 0; i_al < this.areaList.size()-1; i_al++){
				this.differenceList.add( this.areaList.get(i_al+1) - this.areaList.get(i_al) );
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	private void calcParam(){
		//---- 平均、標準偏差、しきい値を求める ----//
		//---- avg_Diff, stdDev_EyeArea, Th1, Th2 ----//

		// 平均を算出
		double sum_avg_diff = 0.0;		// 一時変数
		double sum_avg_eyeArea = 0.0;	// 一時変数
		for(int i_avg = 0; i_avg < this.n_of_sample; i_avg++)	{
			sum_avg_diff += this.differenceList.get(i_avg);
			sum_avg_eyeArea += this.areaList.get(i_avg);
		}
		// 平均は最初の10サンプルから算出する
		this.avg_Diff = sum_avg_diff / this.n_of_sample;
		this.avg_EyeArea = sum_avg_eyeArea / this.n_of_sample;

		// 標準偏差を算出
		double sum_stdDev = 0.0;
		for(int i_stdDev = 0; i_stdDev < this.n_of_sample; i_stdDev++) { 
			double tmp = this.differenceList.get(i_stdDev);
			sum_stdDev += (tmp - this.avg_Diff)*(tmp - this.avg_Diff);
		}
		sum_stdDev /= this.n_of_sample;
		this.stdDev_EyeArea = Math.sqrt(sum_stdDev);

		// しきい値を算出
		this.Th1 = this.avg_Diff + this.sigma*this.stdDev_EyeArea;
		this.Th2 = this.avg_Diff - this.sigma*this.stdDev_EyeArea;
		this.Th_eyeArea = this.avg_EyeArea * this.p;	
	}

	// TODO: 名前の変更
	private void detect(){
		//---- 瞬目の検出 ----//
		// 波形の走査
		for(int i_eb = 0; i_eb < this.differenceList.size(); i_eb++){
			double sample = differenceList.get(i_eb);

			// 瞬目発生の検出
			if(sample <= this.Th2){
				// 後の1サンプルを確認し、しきい値以下であれば誤検出とする
				if( this.differenceList.size() == i_eb+1 ) break;
				if( this.differenceList.get(i_eb+1) > Th2 ) continue;

				int begin = i_eb;								// 瞬目開始サンプルと終了サンプル
				List<Double> ebList = new ArrayList<Double>();	// 一回分の瞬目波形(差分値ではない!)を格納する
				ebList.add(areaList.get(begin)); 				// 差分値波形がしきい値を跨いだら面積を記録

				// TODO: 瞬目検出のアルゴリズムをメソッド化
				// しきい値の前を検出
				for(int k_eb = i_eb; this.areaList.get(k_eb)-this.areaList.get(k_eb+1) > 0; k_eb--){
					ebList.add(0, this.areaList.get(k_eb));
				}

				// しきい値の間を検出
				for(int j_eb = i_eb; j_eb < this.differenceList.size(); j_eb++){ // しきい値間は差分値波形から検出する
					if(this.differenceList.get(j_eb) > Th1) {
						i_eb = j_eb;
						break;
					}
					ebList.add(this.areaList.get(j_eb));
				}

				// しきい値の後を検出
				for(int l_eb = i_eb; this.areaList.get(l_eb)-this.areaList.get(l_eb+1) < 0 || this.areaList.get(l_eb) < this.Th_eyeArea; i_eb = ++l_eb){
					ebList.add(this.areaList.get(l_eb));
				}

				this.eyeBlinksMap.put(begin, ebList);
			}
		}	
	}



	// XXX: 汎用性を高め、綺麗に
	public void print(){
		//---- ファイルへ出力 ----//
		// 確認用出力
		int index = 0;
		try( PrintWriter pw = new PrintWriter(new File(fileName + "_out_check_"+"sigma="+sigma+"_p="+p+".txt")) ){
			pw.println("index : 開始サンプル-終了サンプル : 面積の変化");

			Set<Integer> keySet = eyeBlinksMap.keySet();
			for(int key : keySet){
				List<Double> list = eyeBlinksMap.get(key);

				pw.print(index++ + " : " + key +"-"+ ( key+list.size()-1 ) + " : ");
				for(int i_list = 0; i_list < list.size(); i_list++){
					pw.print(list.get(i_list) + (i_list+1 < list.size()?", ":"") );
				}
				pw.println();
			}

		}catch(Exception e){
			e.printStackTrace();
		}

		//---- ファイルへ出力 ----//
		// 確認用出力
		try( PrintWriter pw = new PrintWriter(new File(fileName + "_out_diff_"+"sigma="+sigma+"_p="+p+".csv")) ){
			for(double d : differenceList)
				pw.println(d);

		}catch(Exception e){
			e.printStackTrace();
		}	

		// マシマシカ用出力
		try( PrintWriter pw = new PrintWriter(new File(fileName + "_out_mathematica_"+"sigma="+sigma+"_p="+p+".txt")) ){
			Set<Integer> keySet = eyeBlinksMap.keySet();
			int keyIndex = 0;
			pw.println("{");

			for(int key : keySet){
				List<Double> list = eyeBlinksMap.get(key);

				pw.print("{");
				for(int i_list = 0; i_list < list.size(); i_list++){
					pw.print("{"+ key++ + ", " + list.get(i_list) + (i_list+1 < list.size()?"}, ":"}") );
				}
				pw.println( (keyIndex+1 < keySet.size()?"},":"}") );
				keyIndex++;
			}
			pw.println("}");

		}catch(Exception e){
			e.printStackTrace();
		}

		// マシマシカ2用出力
		// index, n_of_sample, 開始面積と最低面積の差, 終了面積と最低面積の差
		try( PrintWriter pw = new PrintWriter(new File(fileName + "_out_mathematica2_"+"sigma="+sigma+"_p="+p+".txt")) ){
			Set<Integer> keySet = eyeBlinksMap.keySet();
			int keyIndex = 0;
			pw.println("{");

			for(int key : keySet){
				List<Double> list = eyeBlinksMap.get(key);

				// 最低値を取得
				double min = list.get(0);
				for(double d : list) if(min > d) min = d;

				//				index				サンプル数			 開始と最低の差				終了と最低の差								
				pw.println("{"+ keyIndex++ + ", " + list.size() + ", " + (int)(list.get(0)-min) + ", " + (int)(list.get(list.size()-1) - min) + (keyIndex < eyeBlinksMap.size()?"}, ":"}") );
			}

			pw.println("}");

		}catch(Exception e){
			e.printStackTrace();
		}

		// マシマシカ3用出力
		// index, n_of_sample, 開始面積と最低面積の差, 終了面積と最低面積の差
		try( PrintWriter pw = new PrintWriter(new File(fileName + "_out_mathematica3_"+"sigma="+sigma+"_p="+p+".txt")) ){
			Set<Integer> keySet = eyeBlinksMap.keySet();
			int keyIndex = 0;
			pw.println("{");

			for(int key : keySet){
				List<Double> list = eyeBlinksMap.get(key);

				// 最低値を取得
				double min = list.get(0);
				for(double d : list) if(min > d) min = d;

				//				index				サンプル数			 開始と最低の差				終了と最低の差								
				pw.println("{"+ keyIndex++ + ", " + list.size() + ", " + (int)(list.get(0)-min) + ", " + (int)(list.get(list.size()-1) - min) + (keyIndex < eyeBlinksMap.size()?"}, ":"}") );
			}

			pw.println("}");

		}catch(Exception e){
			e.printStackTrace();
		}

	}
}


